<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */


namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //function for listing all products in jason format
    public function showAllProducts()
    {
        $products = Product::join('category', 'products.category_id', '=', 'category.category_id')->get();
        return response()->json($products);
    }
    //function for showing one product details
    public function showOneProduct($id)
    {
        return response()->json(Product::find($id));
    }
    //function for create the new product
    public function create(Request $request)
    {   //validate the product request
         $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'category_id' => 'required|numeric'
        ]);
        $author = Product::create($request->all());
        return response()->json($author, 201);
    }
    //function for update of the product
    public function update($id, Request $request)
    {
        $author = Product::findOrFail($id);
        $author->update($request->all());
        return response()->json($author, 200);
    }
    //function for soft delete of the product 
    public function delete($id)
    {
        Product::findOrFail($id)->delete();
        return response()->json('Deleted Successfully', 200);
    }
}

